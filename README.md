# Desafio QA - Agriness #

Esse repositório contém testes automatizados de API e E2E conforme solicitado no desafio

### Como executar
Favor, seguir os passos abaixo:

1 - Primeiramente, abra o seu terminal, navegue até a pasta desejada e execute o comando abaixo:
```bash
git clone https://gabrielbesen26@bitbucket.org/gabrielbesen26/automation-test-agriness.git
```

2 - Ao final do download, execute o comando abaixo:
```bash
npm install
```

3 - Após a instalação das dependências, execute o comando abaixo para executar os testes:
```bash
npm run cypress:run
```

### Onde estão os scripts de teste? ###

O script de teste referente aos testes na API você vai encontrar no caminho ```cypress/integration/api/*```

O script de teste referente aos testes E2E você vai encontrar no caminho ```cypress/integration/e2e/*```

### Configuração do ambiente ###
Na raiz do projeto você encontra o arquivo ```cypress.json``` que contém alguns parâmetros do ambiente de teste.
___
## Abertura de Issue

Em caso de abertura de issue, a forma da qual eu enviaria para o time de desenvolvimento seria conforme o template abaixo:

```
Bug - Possibilidade de registrar participantes sem estar logado.

Prioridade: Alta
Data: 07/09/2020
Reportado por: Gabriel Thomaz H. Besen
Versão/Branch: Master
Browser: Chrome/Firefox

Descrição:
Ao remover '/login' da URL ou acessar diretamente a raiz, o usuário consegue acessar a página de cadastro de participante sem estar logado no sistema.

Steps to reproduce:
1 - Acesse a url http://agriness-challenge.herokuapp.com/login
2 - Remova o '/login' da URL ou acesse diretamente http://agriness-challenge.herokuapp.com
3 - Note que é possível registrar um participante sem estar logado no sistema

Resultado esperado:
Ao acessar diretamente a raiz, caso o usuário não esteja logado no sistema, ele deve ser redirecionado para a página de login.

Screenshot: N/A
```