// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add("login", (user, pass) => {
    var formLogin = "form[id='login']";
    cy.get(formLogin).find("#loginEmail").clear().type(user);
    cy.get(formLogin).find("#loginSenha").clear().type(pass);
    cy.get(formLogin).find("#botaoLogin").click();
})

Cypress.Commands.add("clicarEmEditarParticipante", (participante) => {
    cy.xpath(`//tbody/tr[./td[contains(text(), '${participante}')]]/td/button[@id='editarContato']`).click();
})

Cypress.Commands.add("buscarParticipante", (participante) => {
    cy.get("#buscaNome").clear().type(participante);
    cy.get("#botaoBuscar").click();
})

Cypress.Commands.add("excluirParticipante", (participante) => {
    cy.xpath(`//tbody/tr[./td[contains(text(), '${participante}')]]/td/button[@id='deletarContato']`).click();
    cy.on('window:confirm', () => true);
})
