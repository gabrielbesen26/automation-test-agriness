/// <reference types="Cypress" />
const faker = require('faker-br');

const CEAHA_URL = Cypress.env("ceahaUrl");
const CEAHA_USER = "teste.agriness@agriness.com";
const CEAHA_PASS = "agriness";

function visitAndCleanSessionStorage(url) {
    cy.visit(url, {
        onBeforeLoad: (win) => {
            win.sessionStorage.clear()
        }
    });
}

function createPerson(){
    return {
        nome: `${faker.name.firstName()} ${faker.name.lastName()}`,
        cpf: faker.br.cpf(),
        rg: '000555123',
        nascimento: '01011990'
    }
}

describe("Desafio E2E", () => {

    beforeEach("Limpando sessionStorage e acessando url", () => {
        visitAndCleanSessionStorage(CEAHA_URL + "/login");
    })

    it("Cadastrar um usuário, buscar, editar e excluir", () => {
        cy.log("Realizando Login");
        cy.login(CEAHA_USER, CEAHA_PASS);
        cy.contains("Olá, teste.agriness").should('be.visible');
        
        cy.log("Cadastrando novo participante");
        var person = createPerson();
        cy.get("#novoParticipante").click();
        cy.get("#full-name").clear().type(person.nome);
        cy.get("#date-birth").type(person.nascimento);
        cy.get("#rg").type(person.rg);
        cy.get("#cpf").type(person.cpf);
        cy.get("#cadastrar").click();
        cy.wait(3000);
        //cy.contains("Participante cadastrado com sucesso.").should('be.visible');

        cy.log("Buscando o participante e verificando se foi cadastrado");
        cy.visit(`${CEAHA_URL}/home`);
        cy.buscarParticipante(person.nome);
        cy.get(".table").should('contain', person.nome);

        cy.log("Editando o participante")
        cy.clicarEmEditarParticipante(person.nome);
        person = createPerson();
        cy.wait(3000);
        cy.get("#full-name").clear().type(person.nome);
        cy.get("#date-birth").clear().type(person.nascimento);
        cy.get("#nacionality").clear().type("Brasileira");
        cy.get("#city").clear().type("Florianópolis");
        cy.get("#uf").select("SC");
        cy.get("#father-name").clear().type(faker.name.firstName() + " " + faker.name.lastName());
        cy.get("#mother-name").clear().type(faker.name.firstName() + " " + faker.name.lastName());
        cy.get("#orgao-emissor").type("SSP");
        cy.get("#cpf").type(person.cpf);
        cy.get("#inputEstadoCivil").select("Solteiro");
        cy.get("#telefoneRes").clear().type("4812345678");
        cy.get("#telefoneCel").clear().type("48912345678");
        cy.get("#email").clear().type(faker.internet.email());
        cy.get("#editar").click();
        cy.contains("Participante alterado com sucesso.").should('be.visible');

        cy.log("Excluindo participante");
        cy.visit(`${CEAHA_URL}/home`);
        cy.buscarParticipante(person.nome);
        cy.excluirParticipante(person.nome);
        cy.contains("Participante removido com sucesso com sucesso.").should('be.visible');
    })

})