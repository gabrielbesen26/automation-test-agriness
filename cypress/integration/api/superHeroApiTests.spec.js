/// <reference types="Cypress" />

const URL_SUPERHERO = Cypress.env("baseUrl");
const ACCESS_TOKEN = Cypress.env("accessToken");
const HERO_ID = 620;

describe("Desafio API", () => {

    it("Realizar a chamada de API e verificar o response e o tipo de suas propriedades", () => {
        cy.request('GET', `${URL_SUPERHERO}/${ACCESS_TOKEN}/${HERO_ID}`).as('getHero');

        cy.get('@getHero').should((response) => {
            expect(response.status).to.be.equal(200);
            expect(response.headers).to.have.property('content-type').to.be.equal('application/json');
            expect(response.body).to.have.property('response').to.be.a('string');
            expect(response.body).to.have.property('id').to.be.a('string');
            expect(response.body).to.have.property('name').to.be.a('string');
            expect(response.body).to.have.property('powerstats').to.be.a('object');
            expect(response.body).property('powerstats').to.have.property('intelligence').to.be.a('string');
            expect(response.body).property('powerstats').to.have.property('strength').to.be.a('string');
            expect(response.body).property('powerstats').to.have.property('speed').to.be.a('string');
            expect(response.body).property('powerstats').to.have.property('durability').to.be.a('string');
            expect(response.body).property('powerstats').to.have.property('power').to.be.a('string');
            expect(response.body).property('powerstats').to.have.property('combat').to.be.a('string');
            expect(response.body).to.have.property('biography').to.be.a('object');
            expect(response.body).property('biography').to.have.property('full-name').to.be.a('string');
            expect(response.body).property('biography').to.have.property('alter-egos').to.be.a('string');
            expect(response.body).property('biography').to.have.property('aliases').to.be.a('array');
            expect(response.body).property('biography').to.have.property('full-name').to.be.a('string');
            expect(response.body).property('biography').to.have.property('place-of-birth').to.be.a('string');
            expect(response.body).property('biography').to.have.property('first-appearance').to.be.a('string');
            expect(response.body).property('biography').to.have.property('publisher').to.be.a('string');
            expect(response.body).property('biography').to.have.property('alignment').to.be.a('string');
            expect(response.body).to.have.property('appearance').to.be.a('object');
            expect(response.body).property('appearance').to.have.property('gender').to.be.a('string');
            expect(response.body).property('appearance').to.have.property('race').to.be.a('string');
            expect(response.body).property('appearance').to.have.property('height').to.be.a('array');
            expect(response.body).property('appearance').to.have.property('weight').to.be.a('array');
            expect(response.body).property('appearance').to.have.property('eye-color').to.be.a('string');
            expect(response.body).property('appearance').to.have.property('hair-color').to.be.a('string');
            expect(response.body).to.have.property('work').to.be.a('object');
            expect(response.body).property('work').to.have.property('occupation').to.be.a('string');
            expect(response.body).property('work').to.have.property('base').to.be.a('string');
            expect(response.body).to.have.property('connections').to.be.a('object');
            expect(response.body).property('connections').to.have.property('group-affiliation').to.be.a('string');
            expect(response.body).property('connections').to.have.property('relatives').to.be.a('string');
            expect(response.body).to.have.property('image').to.be.a('object');
            expect(response.body).property('image').to.have.property('url').to.be.a('string');
        })
    });

    it("Fazer a chamada de API para o atributo Powerstats e verificar seus valores", () => { 
        cy.request('GET', `${URL_SUPERHERO}/${ACCESS_TOKEN}/${HERO_ID}/powerstats`).as('getHeroPowerstats');
        cy.get('@getHeroPowerstats').should((response) => {
            expect(response.status).to.be.equal(200);
            expect(response.body).to.have.property('response').to.be.equal('success');
            expect(response.body).to.have.property('id').to.be.equal('620');
            expect(response.body).to.have.property('name').to.be.equal('Spider-Man');
            expect(response.body).to.have.property('intelligence').to.be.equal('90');
            expect(response.body).to.have.property('strength').to.be.equal('55');
            expect(response.body).to.have.property('speed').to.be.equal('67');
            expect(response.body).to.have.property('durability').to.be.equal('75');
            expect(response.body).to.have.property('power').to.be.equal('74');
            expect(response.body).to.have.property('combat').to.be.equal('85');
        })
    });

    it("Fazer a chamada de API para o atributo Biography e verificar se Aliases contém o valor esperado", () => {
        cy.request('GET', `${URL_SUPERHERO}/${ACCESS_TOKEN}/${HERO_ID}/biography`).as('getHeroBiography');
        cy.get('@getHeroBiography').should((response) => {
            expect(response.status).to.be.equal(200);
            expect(response.body).to.have.property('response').to.be.equal('success');
            expect(response.body).to.have.property('aliases').to.include('Dusk');
        })
    })

    it("Fazer a chamada de API e verificar a presença do atributo Eye-Color", () => {
        cy.request('GET', `${URL_SUPERHERO}/${ACCESS_TOKEN}/${HERO_ID}`).as('getHero');
        cy.get('@getHero').its('body').should('exist', 'eye-color');
    })

})
